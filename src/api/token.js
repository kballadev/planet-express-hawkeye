import { TOKEN, USER } from "../utils/constants";

function setLocalStorage (key, value) {
  if (typeof window !== "undefined") {
    window.localStorage.setItem(key, value)  
  }
}

function delLocalStorage (key) {
  if (typeof window !== "undefined") {
    window.localStorage.removeItem(key)  
  }
}

function getLocalStorage (key){
  if (typeof window !== "undefined") {
    return window.localStorage.getItem(key);
  } else {
    return null
  }
}

//Tokens
export function setToken(token) {
  setLocalStorage(TOKEN, token);
}

export function getToken() {
  return getLocalStorage(TOKEN);
}

export function removeToken() {
  delLocalStorage(TOKEN);
}

//Users
export function setUser(user) {
  setLocalStorage(USER, user);
}
export function getUser() {
  return getLocalStorage(USER);
}

export function removeUser() {
  delLocalStorage(USER);
}
