import React from "react";

const Pricing = () => {
  return (
    <div className="ui container text ">
      <h1 className="ui header center aligned grey">Get Premium</h1>
      <div className="ui cards three">
        <div className="ui card">
          <div className="content">
            <div className="header center aligned">Free</div>
            <div className="meta center aligned">free and unlimited</div>
            <div className="ui divider horizontal">$0 / month</div>
            <div className="ui list">
              <div className="item">
                <i className="icon checkmark"></i>{" "}
                <div className="content">
                  Full <b>free</b>
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark"></i>{" "}
                <div className="content">Established Searches</div>
              </div>
              <div className="item">
                <i className="icon checkmark"></i>{" "}
                <div className="content">
                  Automatic <b>Degree</b> and <b>Score</b>
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark"></i>{" "}
                <div className="content">
                  <b>5</b> Searches per day
                </div>
              </div>
            </div>
          </div>
          <div className="extra content">
            <button className="ui button fluid  disabled">Default</button>
          </div>
        </div>
        <div className="ui card raised">
          <div className="content">
            <a className="ui label left corner ">
              <i className="icon plus black"></i>
            </a>
            <div className="header center aligned">Premium</div>
            <div className="meta center aligned">prio ressources</div>
            <div className="ui divider horizontal">€69 / month</div>
            <div className="ui list">
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  Price <b>per</b> user
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Allow you</b> to create user groups
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Select</b> your own crawlers
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Degree</b> and <b>Score</b> manual
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>API REST</b> from thirds
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Export</b> your data
                  <br />
                  <small>(PDF)</small>
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Support</b> us
                </div>
              </div>
            </div>
          </div>
          <div className="extra content">
            <button className="ui button fluid ">Order now</button>
          </div>
        </div>
        <div className="ui card raised">
          <div className="content">
            <a className="ui label left corner ">
              <i className="icon plus black"></i>
            </a>
            <div className="header center aligned">Custom</div>
            <div className="meta center aligned">prio ressources</div>
            <div className="ui divider horizontal">$X / month</div>
            <div className="ui list">
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>All</b> the premium perks
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Custom</b> price
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>120</b> days of history
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>100</b> is just a number
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>60</b> is another number
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Awesome</b> Server
                  <br />
                  <small>(50 Petaflops/s)</small>
                </div>
              </div>
              <div className="item">
                <i className="icon checkmark "></i>{" "}
                <div className="content">
                  <b>Support</b> us
                </div>
              </div>
            </div>
          </div>
          <div className="extra content">
            <button className="ui button fluid ">Order now</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Pricing;
