import { useState, forwardRef, useImperativeHandle } from 'react'
import { BASE_PATH } from "../../utils/constants";
import { getToken } from "../../api/token";
import Table from "@material-ui/core/Table";
import { withStyles } from '@material-ui/core/styles';
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";


const StyledTableRow = withStyles((theme) => ({
  root: {
    borderBottom: "2px solid rgb(33, 41, 41)",
    '&:nth-of-type(odd)': {
      backgroundColor: "#1C2222",
    },
  },
}))(TableRow);

const urlBase = `${BASE_PATH}/api/graph/urls/`;
const params = {
  method: "GET",
  headers: {
    Authorization: "Bearer " + getToken(),
    "Content-Type": "application/json",
    Accept: "application/json",
  }
};

function getURLData(urlsData, urlId) {
  return urlsData.find(url => url[0] === urlId)
};

function UrlTable(props, ref) {
  const [neighbours, setNeighbours] = useState([]);

  useImperativeHandle(ref, () => ({
    async setUrlTable(links, topic) {
      const responseData = await getUrlInfo(links, topic);
      setNeighbours(responseData);
    }
  }), [])

  async function getUrlInfo(links, topic) {


    var neighbours = [];
    var urls = [];
    for (const link of links) {
      var neighbour = null;

      if (link.source.topic == topic && link.target.topic != topic) {
        neighbour = link.target.topic;
      }
      if (link.target.topic == topic && link.source.topic != topic) {
        neighbour = link.source.topic;
      }
      if (neighbour != null) {
        urls = [...new Set([...urls, ...link.urls])];
        neighbours.push({
          neighbour: neighbour,
          urls: link.urls
        })
      }
    };

    if (urls.length) {
      var urlEndpoint = `${urlBase}${urls}`;
      var urlsResponse = await fetch(urlEndpoint, params);
      var urlsJson = await urlsResponse.json();

      neighbours.forEach(neighbour => {
        neighbour.urls = neighbour.urls.map(url => {
          var urlData = getURLData(urlsJson.urls, url);
          return {
            id: urlData[0],
            url: urlData[1],
            title: (urlData[2]) ? (urlData[2]) : (urlData[1]),
            autors: urlData[3]
          }

        })
      })
    }

    return neighbours;
  }

  return (
    <>
      <TableContainer className="TableContinerSyled" component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className="HeaderTableCell">
                Related Topics
              </TableCell>
              <TableCell className="HeaderTableCell">
                Papers/Urls
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {neighbours.map((neighbour, index) => (
              <StyledTableRow key={index}>
                <TableCell className="TopicTableCell">
                  {neighbour.neighbour}
                </TableCell>
                <TableCell className="PaperTableCell">
                  {neighbour.urls.map(function (url, index) {
                    return (
                      <>
                        <a target="_blank" style={{ color: "white" }} href={url.url} data-toggle="tooltip" title={url.title}>
                          {url.title}
                        </a>
                        <br></br>
                      </>
                    );
                  })}
                </TableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );

}
export default forwardRef(UrlTable)