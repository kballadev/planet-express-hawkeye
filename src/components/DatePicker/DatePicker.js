import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { DateRange } from 'react-date-range';

function DatePicker(props) {
    function handleSelect(ranges) {
        props.setDateInput([ranges.selection]);
    };

    return (
        <DateRange
            className="DatePickerStyle"
            editableDateInputs={true}
            onChange={handleSelect}
            moveRangeOnFirstSelection={false}
            ranges={props.dateInput}>
        </DateRange>
    );
}

export default DatePicker