import { Grid, Image } from "semantic-ui-react";
import TypeIt from "typeit-react";
import Footer from "../Footer/Footer";

const HomePage = () => {
  return (
    <div className="home">
      <BackgroundVideo />
      <div className="home__filter">
        <Grid className="home">
          <Grid.Row className="home__logo">
            <Logo />
          </Grid.Row>
          <Grid.Row className="home__text">
            <TypeIt element={"h1"} className="home__text__motivational">
              We look for what you want, how you need it.
            </TypeIt>
            <h1 className="home__text__p">
              Search and connect ideas differently with our platform API
            </h1>
          </Grid.Row>
        </Grid>
      </div>
    </div>
  );
};
function Logo() {
  return (
    <Image
      src="/logo/planet_express_vertical_white_transparent_noshadow.png"
      alt="Planet-Express"
    />
  );
}

function BackgroundVideo() {
  return (
    <video src="/video/Earth.mp4" autoPlay="autoplay" loop="loop" muted></video>
  );
}

export default HomePage;
