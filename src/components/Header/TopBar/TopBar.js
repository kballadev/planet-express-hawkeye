import { Container, Grid, Image, Button } from "semantic-ui-react";
import useAuth from "../../../hooks/useAuth";
import Link from "next/link";

const TopBar = () => {
  const { auth, logout } = useAuth();

  return (
    <div className="top-bar">
      <Container>
        <Grid className="top-bar">
          <Grid.Column width={8} className="top-bar__left">
            <Logo />
          </Grid.Column>
          <Grid.Column width={8} className="top-bar__right">
            {auth?.user ? (
              <>
                <Button className="buttonUserStyled"
                  content={auth?.user}
                  label="Welcome"
                  labelPosition="left"
                />
                <Link href="/product">
                  <a className="productButton">
                    <Button className= "headerButtonStyled"
                      content="Product"
                      icon="world"
                      labelPosition="left"
                    />
                  </a>
                </Link>
                <Button className= "headerButtonStyled"
                  content="Logout"
                  icon="sign-out alternate"
                  labelPosition="left"
                  onClick={logout}
                />
              </>
            ) : (
              <Link href="/login">
                <a>
                  <Button className= "headerButtonStyled"
                    content="Login"
                    icon="sign-in alternate"
                    labelPosition="left"
                  />
                </a>
              </Link>
            )}
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
};

function Logo() {
  return (
    <Link href="/">
      <a>
        <Image src="/logo/nav_logo_horizontal.png" alt="Planet Express" />
      </a>
    </Link>
  );
}

export default TopBar;
