import TopBar from "./TopBar";
import useAuth from "../../hooks/useAuth";

const Header = () => {
  const { auth } = useAuth();
  return (
    <div className="headerMenu">
      <TopBar />
    </div>
  );
};

export default Header;
