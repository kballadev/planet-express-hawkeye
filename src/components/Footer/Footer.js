import { Image, Grid, Container, Icon } from "semantic-ui-react";
import Link from "next/link";
const Footer = () => {
  return (
    <div className="footer">
      <Container>
        <Grid className="footer">
          <Grid.Column width={4} className="footer__left">
            <Logo />
          </Grid.Column>
          <Grid.Column width={8} className="footer__text">
            <p className="footer__text__copyright">
              &copy; 2020 Kballa.dev All rights reserved
            </p>
          </Grid.Column>
          <Grid.Column width={4} className="footer__right">
            <SocialIcons />
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
};

function Logo() {
  return (
    <Image
      src="/logo/planet_express_vertical_white_transparent_noshadow.png"
      alt="Logo"
    />
  );
}

function SocialIcons() {
  return (
    <div className="socialIcons">
      <Link href="https://twitter.com/KballaDev">
        <a target="_blank">
          <Icon name="twitter" />
        </a>
      </Link>
      <Link href="https://www.linkedin.com/company/kballadev/">
        <a target="_blank">
          <Icon name="linkedin" />
        </a>
      </Link>
    </div>
  );
}

export default Footer;
