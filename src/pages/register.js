import {
  Button,
  Form,
  Grid,
  Header as HeaderLogin,
  Image,
  Message,
  Segment,
} from "semantic-ui-react";
import Link from "next/link";

const RegisterForm = () => (
  <div className="register">
    <Grid
      textAlign="center"
      style={{
        height: "100vh",
      }}
      verticalAlign="middle"
    >
      <Grid.Column
        style={{
          maxWidth: 450,
        }}
      >
        <Logo />
        <HeaderLogin
          as="h2"
          color="teal"
          textAlign="center"
          style={{
            boxShadow: "none",
          }}
        >
          Register your account
        </HeaderLogin>
        <Form size="large">
          <Segment stacked>
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              placeholder="Username"
            />
            <Form.Input
              fluid
              icon="mail"
              iconPosition="left"
              placeholder="E-mail address"
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Confirm password"
              type="password"
            />
            <Button color="teal" fluid size="large">
              Login
            </Button>
          </Segment>
        </Form>
        <Message>
          Already register?
          <Link href="/login">
            <a>Login</a>
          </Link>
        </Message>
      </Grid.Column>
    </Grid>
  </div>
);

function Logo() {
  return (
    <Image src="/logo/planet_express_vertical_white_transparent_noshadow.png" />
  );
}

export default RegisterForm;
