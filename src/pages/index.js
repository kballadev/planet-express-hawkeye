import BasicLayout from "../layouts/BasicLayout";
import HomePage from "../components/Home/Home";
import Footer from "../components/Footer";
import Pricing from "../components/Pricing/Pricing";

export default function Home() {
  return (
    <div className="home">
      <BasicLayout title="Planet-Express">
        <HomePage />

        <Footer />
      </BasicLayout>
    </div>
  );
}
