import { useState, useEffect } from "react";
import "../scss/global.scss";
import AuthContext from "../context/AuthContext";
import { useRouter } from "next/router";
import "semantic-ui-css/semantic.min.css";
import {
  setToken,
  getToken,
  getUser,
  setUser,
  removeToken,
  removeUser,
} from "../api/token";
import { useMemo } from "react";

function MyApp({ Component, pageProps }) {
  const [auth, setAuth] = useState(undefined);
  const [reloadUser, setReloadUser] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const token = getToken();
    const user = getUser();
    if (token || user) {
      setAuth({
        token: token,
        user: user,
      });
    } else {
      setAuth(null);
    }
    setReloadUser(false);
  }, [reloadUser]);

  const login = (token, user) => {
    setToken(token);
    setUser(user);
    setAuth({
      token: token,
      user: user,
    });
  };

  const logout = () => {
    removeToken();
    removeUser();
    setAuth(null);
    router.push("/");
  };

  const authData = useMemo(
    () => ({
      auth: auth,
      login: login,
      logout: logout,
      setReloadUser: () => null,
    }),
    [auth]
  );

  if (auth === undefined) return null;

  return (
    <AuthContext.Provider value={authData}>
      <Component {...pageProps} />
    </AuthContext.Provider>
  );
}

export default MyApp;
