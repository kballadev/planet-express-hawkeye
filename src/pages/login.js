import { useState } from "react";
import Swal from "sweetalert2";
import { useRouter } from "next/router";
import { loginApi } from "../api/user";
import useAuth from "../hooks/useAuth";
import {
  Button,
  Form,
  Grid,
  Header as HeaderLogin,
  Image,
  Segment,
} from "semantic-ui-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Head from "next/head";

const Login = () => {
  const [loading, setLoading] = useState(false);
  const { login } = useAuth();
  const router = useRouter();

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      const response = await loginApi(formData);
      if (response?.access) {
        login(response?.access, formData.username);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Welcome to Planet-Express",
          showConfirmButton: false,
          timer: 2000,
        });
        router.push("/product");
      } else {
        Swal.fire({
          position: "center",
          title: "Login Failed",
          showConfirmButton: false,
          icon: "error",
          timer: 2000,
        });
      }
      setLoading(false);
    },
  });

  return (
    <div className="login">
      <Head>
        <title>Sign In</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Logo />
          <div className="login__logo">
            <HeaderLogin
              as="h2"
              color="teal"
              textAlign="center"
              style={{ marginBottom: "15px" }}
            >
              Log-in to your account
            </HeaderLogin>
          </div>
          <Form size="large" onSubmit={formik.handleSubmit}>
            <Segment stacked>
              <Form.Input
                fluid
                icon="mail"
                iconPosition="left"
                placeholder="E-mail address"
                onChange={formik.handleChange}
                name="username"
                type="text"
                error={formik.errors.email}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                onChange={formik.handleChange}
                name="password"
                error={formik.errors.password}
              />
              <Button
                color="teal"
                fluid
                size="large"
                type="submit"
                loading={loading}
              >
                Login
              </Button>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    </div>
  );
};

function Logo() {
  return (
    <Image
      src="/logo/planet_express_vertical_white_transparent_noshadow.png"
      alt="Logo"
    />
  );
}

function initialValues() {
  return {
    username: "",
    password: "",
  };
}

function validationSchema() {
  return {
    username: Yup.string().email(true).required(true),
    password: Yup.string().required(true),
  };
}

export default Login;
