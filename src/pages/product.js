import React, { useState, useRef, useEffect } from "react";
import { BASE_PATH } from "../utils/constants";
import { getToken } from "../api/token";
import BasicLayout from "../layouts/BasicLayout";
import DatePicker from "../components/DatePicker";
import format from "date-fns/format";
import UrlTable from "../components/UrlTable";
import useAuth from "../hooks/useAuth";
import Swal from "sweetalert2";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";


const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

import {
  Grid,
  Input,
  Segment,
  Button,
  Dropdown,
  Form,
  Popup,
  Header,
} from "semantic-ui-react";
import * as d3 from "d3";

const degreeOptions = [
  { key: "1", value: 1, text: "1" },
  { key: "2", value: 2, text: "2" },
  { key: "3", value: 3, text: "3" },
  { key: "4", value: 4, text: "4" }
];

const degreesNodeClasses = {
  0: "nodeZero",
  1: "nodeOne",
  2: "nodeTwo",
  3: "nodeThree",
  4: "nodeFour"
};

const degreesLabelClasses = {
  0: "labelZero",
  1: "labelOne",
  2: "labelTwo",
  3: "labelThree",
  4: "labelFour"
};

const defaultScore = 1.4;
const minScore = 0.9;
const maxScore = 5.0;
const stepScore = 0.1;

const Product = () => {
  const [inputSearch, setInputSearch] = useState("");
  const [degreeInput, setDegreeInput] = useState(2);
  const [scoreMinMax, setScoreMinMax] = useState({
    min: defaultScore,
    max: maxScore
  });
  const { logout } = useAuth();

  const [scoreLabel, setScoreLabel] = useState(`Score: ${defaultScore} - ${maxScore}`);
  const [degreeLabel, setDegreeLabel] = useState(`Degree: 2`);
  const [dateLabel, setDateLabel] = useState(`Date`);


  const urlTableRef = useRef();
  const [dateInput, setDateInput] = useState([
    {
      startDate: new Date(2015,1,1),
      endDate: new Date(2021,11,12),
      key: 'selection'
    }
  ]);

  useEffect(() => {
    setDateLabel(`Dates: ${format(dateInput[0].startDate, 'dd/MM/yy')} to ${format(dateInput[0].endDate, 'dd/MM/yy')}`);
  }, [dateInput]);

  useEffect(() => {
    setDegreeLabel(`Degree: ${degreeInput}`);
  }, [degreeInput]);

  const onSliderChange = (value) => {
    setScoreMinMax({
      min: value[0],
      max: value[1],
    });
    setScoreLabel(`Score: ${value[0]} - ${value[1]}`);
  };

  const d3Ref = useRef(null);

  const graphD3 = () => {
    function update() {
      d3.selectAll("g > *").remove();
    }
    update();

    var zoom = d3
      .zoom()
      .scaleExtent([0.01, 20])
      .on("zoom", function (event) {
        d3.selectAll("g").attr("transform", event.transform);
      });

    var svg = d3
      .select(d3Ref.current)
      .attr("width", "100%")
      .attr("height", "100%")
      .call(zoom);

    var simulation = d3
      .forceSimulation()
      .force(
        "center",
        d3
          .forceCenter()
          .x(document.getElementById("svgGraph").clientWidth / 2)
          .y(document.getElementById("svgGraph").clientHeight / 2)
      )
      .force("collision", d3.forceCollide(400));

    function dragstarted(event) {
      if (!event.active) simulation.alphaTarget(0.04).restart();
      event.subject.fx = event.subject.x;
      event.subject.fy = event.subject.y;
    }

    function dragged(event) {
      event.subject.fx = event.x;
      event.subject.fy = event.y;
    }

    function dragended(event) {
      if (!event.active) simulation.alphaTarget(0);
      event.subject.fx = null;
      event.subject.fy = null;
    }
    async function GetTopicsApi(inputSearch, degreeInput, scoreMinMax) {
      svg.selectAll('*').remove();
      urlTableRef.current.setUrlTable([], "");
      try {
        const startDate = dateInput[0].startDate.getTime()/1000;
        const endDate = dateInput[0].endDate.getTime()/1000;
        const url = `${BASE_PATH}/api/graph/topics/${inputSearch}/degrees/${degreeInput}/score_min/${scoreMinMax.min}/score_max/${scoreMinMax.max}/date_min/${startDate}/date_max/${endDate}`;
        const params = {
          method: "GET",
          headers: {
            Authorization: "Bearer " + getToken(),
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        };

        const response = await fetch(url, params);
        const graph = await response.json();
        if (response.status === 400) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: graph.message,
            confirmButtonColor: "#66ffd5",
          });
          return null;
        } else if (response.status === 401) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: graph.message,
            confirmButtonColor: "#66ffd5",
          });
          logout();
          return null;
        } else if (response.status !== 200) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: graph.message,
            confirmButtonColor: "#66ffd5",
          });
          return null;
        } else if (graph.nodes.length === 0) {
          Swal.fire({
            icon: "question",
            title: "Oops...",
            text: "Empty Search",
            confirmButtonColor: "#66ffd5",
          });
          return null;
        }

        simulation.nodes(graph.nodes);
        simulation.force(
          "link",
          d3.forceLink(graph.links).id((d) => d.topic)
        );

        const link = svg
          .append("g")
          .attr("stroke", "#25E6C2")
          .attr("stroke-opacity", 0.6)
          .selectAll("line")
          .data(graph.links)
          .join("line")
          .attr("stroke-width", function (d) {
            return (1 + Math.log2(d.score)) * 2.5;
          });

        var node = svg
          .append("g")
          .attr("stroke", "#25E6C2")
          .attr("stroke-width", 1.5)
          .attr("class", "nodes")
          .selectAll("circle")
          .data(graph.nodes)
          .enter()
          .append("circle")
          .attr("r", function (d) {
            return 30 + 10 * (Math.log2(graph.nodes_attr[d.id].urls) + 5);
          })
          .attr("class", "circle")
          .attr("class", function (d) {
            return degreesNodeClasses[d.degree];
          })
          .style("cursor", "pointer")
          .attr("fill", "#252f2f")
          .call(
            d3
              .drag()
              .on("start", dragstarted)
              .on("drag", dragged)
              .on("end", dragended)
          )
          .on("click", function (event, d) {
            urlTableRef.current.setUrlTable(
              graph.links,
              d.topic);
          });

        var labels = svg
          .append("g")
          .attr("class", "label")
          .attr("text-anchor", "middle")
          .selectAll("circle")
          .data(graph.nodes)
          .enter()
          .append("text")
          .attr("class", "labelText")
          .attr("dx", 0)
          .attr("dy", ".35em")
          .style("font-size", function (d) {
            return (
              (2 * (30 + 10 * (Math.log2(graph.nodes_attr[d.id].urls) + 5))) /
              d.topic.length
            );
          })
          .attr("class", function (d) {
            return degreesLabelClasses[d.degree];
          })
          .style("cursor", "pointer")
          .text(function (d) {
            return d.topic.toUpperCase();
          })
          .call(
            d3
              .drag()
              .on("start", dragstarted)
              .on("drag", dragged)
              .on("end", dragended)
          )
          .on("click", function (event, d) {
            urlTableRef.current.setUrlTable(
              graph.links,
              d.topic);
          });

        simulation.on("tick", () => {
          link
            .attr("x1", (d) => d.source.x)
            .attr("y1", (d) => d.source.y)
            .attr("x2", (d) => d.target.x)
            .attr("y2", (d) => d.target.y);
          node.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
          labels
            .attr("x", function (d) {
              return d.x;
            })
            .attr("y", function (d) {
              return d.y;
            });
        });
      } catch (error) {
        console.log("ERROR MESSAGE");
        console.log(error);
        return null;
      }
    }
    GetTopicsApi(inputSearch, degreeInput, scoreMinMax);
  };

  return (
    <BasicLayout className="product">
      <Grid width={16}>
        <Grid.Column width={4}>
          <Segment className="URLTable">
            <UrlTable ref={urlTableRef} />
          </Segment>
        </Grid.Column>
        <Grid.Column width={12}>
          <Grid.Row>
            <Segment className="product__search__container">
              <Form className="product__search">
                <Form.Field inline className="FilterStyle">
                  <Input
                    placeholder="Type to search..."
                    onChange={(e) => {
                      setInputSearch(e.target.value);
                    }}
                  />
                </Form.Field>
                <Form.Field inline className="FilterStyle">
                  <Popup
                    trigger={
                      <Button className="ButtonStyle">
                        {degreeLabel}
                      </Button>
                    }
                    position="bottom center"
                    on="click"
                    pinned
                  >
                    <Grid centered divided columns={1}>
                      <Grid.Column textAlign="center">
                        <Header as="h4">Select a Degree</Header>
                        <Dropdown
                          placeholder="Degree"
                          search
                          selection
                          options={degreeOptions}
                          onChange={(e, data) => {
                            setDegreeInput(data.value);
                          }}
                        />
                      </Grid.Column>
                    </Grid>
                  </Popup>
                </Form.Field>
                <Form.Field inline className="FilterStyle">
                  <Popup
                    trigger={
                      <Button className="ButtonStyle">
                        {scoreLabel}
                      </Button>
                    }
                    position="bottom center"
                    on="click"
                    pinned
                  >
                    <Grid centered divided columns={1}>
                      <Grid.Column textAlign="center">
                        <Header as="h4">Select Score</Header>
                        <Range
                          className={"ui"}
                          defaultValue={[defaultScore, maxScore]}
                          min={minScore}
                          max={maxScore}
                          onChange={onSliderChange}
                          step={stepScore}
                          tipFormatter={(value) => value}
                          style={{ width: "200px" }}
                        />
                      </Grid.Column>
                    </Grid>
                  </Popup>
                </Form.Field>
                <Form.Field inline className="FilterStyle">
                  <Popup
                    trigger={
                      <Button className="ButtonStyle">
                        {dateLabel}
                      </Button>
                    }
                    position="bottom center"
                    on="click"
                    pinned
                  >
                    <DatePicker dateInput={dateInput} setDateInput={setDateInput}>
                    </DatePicker>
                  </Popup>
                </Form.Field>
                <Form.Field inline className="FilterStyle">
                  <Button className="ButtonSearch" type="submit" onClick={(e) => graphD3()}>
                    Search
                  </Button>
                </Form.Field>
              </Form>
            </Segment>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Segment className="svgContainer">
                <svg id="svgGraph" ref={d3Ref}></svg>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid.Column>
      </Grid>
    </BasicLayout>
  );
};

export default Product;
