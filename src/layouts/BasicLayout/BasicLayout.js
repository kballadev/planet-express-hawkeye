import { Container } from "semantic-ui-react";
import classNames from "classnames";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Head from "next/head";

const BasicLayout = (props) => {
  const { children, title, className } = props;
  return (
    <Container
      fluid
      className={classNames("basic-layout", {
        [className]: className,
      })}
    >
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Container fluid className="contentWeb">
        {children}
      </Container>
    </Container>
  );
};

export default BasicLayout;
